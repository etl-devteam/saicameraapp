package it.univet.visionar.cameraappetl;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Bundle;

import android.graphics.SurfaceTexture;
import android.hardware.usb.UsbDevice;
import android.os.IBinder;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Pair;

import com.serenegiant.common.BaseActivity;
import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.USBMonitor.OnDeviceConnectListener;
import com.serenegiant.usb.USBMonitor.UsbControlBlock;
import com.serenegiant.usb.UVCCamera;
import com.serenegiant.usbcameracommon.UVCCameraHandler;
import com.serenegiant.usbcameracommon.UvcCameraDataCallBack;
import com.serenegiant.widget.UVCCameraTextureView;

import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import it.univet.visionar.visionarservice.VisionARService;

/**
 * Visualize the camera, commented code is related to management of multiple camera
 */
public class MainActivity extends BaseActivity implements CameraDialog.CameraDialogParent {
    private static final boolean DEBUG = false;
    private static final String TAG = "MainActivity";

    private static final float[] BANDWIDTH_FACTORS = {0.5f, 1.0f};
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    // for accessing USB and USB camera
    private USBMonitor mUSBMonitor;
    private UsbControlBlock monitorSaved;
    private UsbDevice deviceSaved;
    private boolean defaultValue = true;

    private LinearLayout myLayout;
    private UVCCameraHandler mHandlerFirst;
    private UVCCameraTextureView mUVCCameraViewFirst;
    private ImageButton mCaptureButtonFirst;
    private SurfaceTexture mFirstPreviewSurface;
    private ImageView mUpdateArrow;
    private Button closeButton;

    private Button getResolutionButton;

    private SeekBar contrBar, brightBar;

    private int SET_WIDTH = UVCCamera.DEFAULT_PREVIEW_WIDTH;
    private int SET_HEIGHT = UVCCamera.DEFAULT_PREVIEW_HEIGHT;

    private String selectedResolution = "";
    private final Map<String, Pair<Integer, Integer>> resolutionsAvailable = new HashMap<>();
    private AlertDialog resolutionDialog;
    private Button buttonPositiveInvolvement;
    private int SELECTED_WIDTH = 800;
    private int SELECTED_HEIGHT = 600;

    VisionARService VARService = null;
    boolean mBound             = false;

    private final ServiceConnection binderConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();
            mBound = true;

            VARService.setTouchpadListener(ev -> {
                if (ev == VisionARService.touchpadEvent.SINGLE_TAP) {
                    Log.d("act list ", "Single Tap");
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Single Tap", Toast.LENGTH_SHORT).show());
                } else if (ev == VisionARService.touchpadEvent.DOUBLE_TAP) {
                    Log.d("act list ", "Double Tap");
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Double Tap", Toast.LENGTH_SHORT).show());
                } else if (ev == VisionARService.touchpadEvent.SWIPE_DOWN) {
                    Log.d("act list ", "Swipe Down");
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Swipe Down", Toast.LENGTH_SHORT).show());
                } else {  // VisionARService.touchpadEvent.SWIPE_UP
                    Log.d("act list ", "Swipe Up");
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Swipe Up", Toast.LENGTH_SHORT).show());
                }
            });

            VARService.StartTouchpad();

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        startService(intent);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }

        initView();
        initData();
        displayResolutionDialog();
        //initCamera(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT);
    }

    private void initView() {
        mUVCCameraViewFirst = findViewById(R.id.camera_view_first);
        mCaptureButtonFirst = findViewById(R.id.capture_button_first);
        mUpdateArrow        = findViewById(R.id.update_camera);
        closeButton         = findViewById(R.id.closeCamera_btn);
        myLayout            = findViewById(R.id.linLayout);

        contrBar            = findViewById(R.id.contrSeekBar);
        brightBar           = findViewById(R.id.brightSeekBar);

        getResolutionButton = findViewById(R.id.getResolutionButton);

        mUVCCameraViewFirst.setOnClickListener(mOnClickListener);
        mCaptureButtonFirst.setOnClickListener(mOnClickListener);
        closeButton.setOnClickListener(mOnClickListener);
        myLayout.setOnClickListener(mOnClickListener);
        getResolutionButton.setOnClickListener(mOnClickListener);

        contrBar.setOnSeekBarChangeListener(contrErGlassChangeListener);
        brightBar.setOnSeekBarChangeListener(brightErGlassChangeListener);

        mUpdateArrow.setVisibility(View.VISIBLE);
        mCaptureButtonFirst.setVisibility(View.INVISIBLE);
    }

    private void initData() {

        mUSBMonitor = new USBMonitor(this, mOnDeviceConnectListener);

        resolutionsAvailable.put("320x240", new Pair<>(320, 240));
        resolutionsAvailable.put("640x480", new Pair<>(640, 480));
        resolutionsAvailable.put("800x600", new Pair<>(800, 600));
        resolutionsAvailable.put("1024x768", new Pair<>(1024, 768));
        resolutionsAvailable.put("1280x960", new Pair<>(1280, 960));
    }

    private void initCamera(Integer width, Integer height) {

        // Set width and height of display
        //mUVCCameraViewFirst.setAspectRatio(width / (float) height);

        // Set resolution as user choice
        SELECTED_WIDTH = width;
        SELECTED_HEIGHT = height;

        // Set width and height of camera resolution
        try{
            mHandlerFirst = UVCCameraHandler.createHandler(this, mUVCCameraViewFirst
                    , width, height, BANDWIDTH_FACTORS[0], firstDataCallBack);
        } catch (Exception ex) {
            Log.d("@ @ @ ", "Exception: " + ex);
        }

        /*if(defaultValue){
            Log.d("@ @ @ ", "DEFAULT Resolution: " + mHandlerFirst.getWidth() + "x" + mHandlerFirst.getHeight());
            Toast.makeText(MainActivity.this, "DEFAULT Resolution:\n" + mHandlerFirst.getWidth() + "x" + mHandlerFirst.getHeight(), Toast.LENGTH_SHORT).show();
            defaultValue = false;
        } else {
            Log.d("@ @ @ ", "Width: " + mHandlerFirst.getWidth() + ", Height: " + mHandlerFirst.getHeight());
            Toast.makeText(MainActivity.this, "Resolution: " + mHandlerFirst.getWidth() + "x" + mHandlerFirst.getHeight(), Toast.LENGTH_SHORT).show();
        }*/
    }

    SeekBar.OnSeekBarChangeListener contrErGlassChangeListener = new SeekBar.OnSeekBarChangeListener() {
        int myProgress_contrast = 120;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            myProgress_contrast = progress;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (mHandlerFirst.isOpened()) {
                mHandlerFirst.setValue(UVCCamera.PU_CONTRAST, myProgress_contrast);
                Toast.makeText(MainActivity.this, "Contrast value: " + myProgress_contrast, Toast.LENGTH_SHORT).show();
            } else Toast.makeText(MainActivity.this, "Camera is not active", Toast.LENGTH_SHORT).show();
        }
    };

    SeekBar.OnSeekBarChangeListener brightErGlassChangeListener = new SeekBar.OnSeekBarChangeListener() {
        int myProgress_brightness = 120;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            myProgress_brightness = progress;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (mHandlerFirst.isOpened()) {
                mHandlerFirst.setValue(UVCCamera.PU_BRIGHTNESS, myProgress_brightness);
                Toast.makeText(MainActivity.this, "Brightness value: " + myProgress_brightness, Toast.LENGTH_SHORT).show();
            } else Toast.makeText(MainActivity.this, "Camera is not active", Toast.LENGTH_SHORT).show();
        }
    };

    UvcCameraDataCallBack firstDataCallBack = data -> {
        if (VARService.WriteImg(ConvertToBmp(data, SELECTED_WIDTH, SELECTED_HEIGHT), VisionARService.includeBattery.PLAIN) == VisionARService.writeStatus.SUCCESS)
            Log.v(TAG, "WriteImg SUCCESS");
        else Log.v(TAG, "Error on VisionAR WriteImg");
    };

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {

            view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

            if (view == mUVCCameraViewFirst) {
                if (mHandlerFirst != null) {
                    if (!mHandlerFirst.isOpened()) {
                        CameraDialog.showDialog(MainActivity.this);
                    }
                }
            } else if (view == mCaptureButtonFirst) {
                CaptureImage();
            } else if (view == closeButton) {
                mHandlerFirst.close();
            } else if (view == getResolutionButton) {
                if(mHandlerFirst.isOpened()){
                    Toast.makeText(MainActivity.this, "Resolution: " + mHandlerFirst.getWidth() + "x" + mHandlerFirst.getHeight(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Camera is not active", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private final OnDeviceConnectListener mOnDeviceConnectListener = new OnDeviceConnectListener() {
        @Override
        public void onAttach(final UsbDevice device) {
            //Toast.makeText(MainActivity.this, "USB_DEVICE_ATTACHED", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onConnect(final UsbDevice device, final UsbControlBlock ctrlBlock, final boolean createNew) {
            //Device successfully connected
            if (DEBUG) Log.v(TAG, "onConnect:" + device);
            if (mUSBMonitor.hasPermission(device)) {
                if (!mHandlerFirst.isOpened()) {
                    monitorSaved = ctrlBlock;       // it is a copy for re-load cameraHandler
                    deviceSaved = device;           // it is a copy for re-load cameraHandler
                    mHandlerFirst.open(ctrlBlock);

                    mFirstPreviewSurface = mUVCCameraViewFirst.getSurfaceTexture();
                    mHandlerFirst.startPreview(mFirstPreviewSurface);

                    runOnUiThread(() -> {
                        mUpdateArrow.setVisibility(View.INVISIBLE);
                        mCaptureButtonFirst.setVisibility(View.VISIBLE);
                    });

                    if (mHandlerFirst != null && mHandlerFirst.isOpened()) {
                        contrBar.setProgress(mHandlerFirst.getValue(UVCCamera.PU_CONTRAST));
                        brightBar.setProgress(mHandlerFirst.getValue(UVCCamera.PU_BRIGHTNESS));
                    }
                }
            }
        }

        @Override
        public void onDisconnect(final UsbDevice device, final UsbControlBlock ctrlBlock) {
            if (DEBUG) Log.v(TAG, "onDisconnect:" + device);
            if ((mHandlerFirst != null) && !mHandlerFirst.isEqual(device)) {
                Log.d("@ @ @ ", "onDISCONNECT");
                queueEvent(() -> {
                    mHandlerFirst.close();
                    if (mFirstPreviewSurface != null) {
                        mFirstPreviewSurface.release();
                        mFirstPreviewSurface = null;
                    }
                    setCameraButton();
                }, 0);
            }
        }

        @Override
        public void onDettach(final UsbDevice device) {
            if (DEBUG) Log.v(TAG, "onDettach:" + device);
            Toast.makeText(MainActivity.this, "USB_DEVICE_DETACHED", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(final UsbDevice device) {
            if (DEBUG) Log.v(TAG, "onCancel:");
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mUSBMonitor.register();
        if (mUVCCameraViewFirst != null)
            mUVCCameraViewFirst.onResume();
    }

    @Override
    protected void onStop() {
        Log.d("@ @ @ ", "onSTOP");
        mHandlerFirst.close();

        if (mBound) VARService.StopTouchpad();

        if (mUVCCameraViewFirst != null)
            mUVCCameraViewFirst.onPause();
        mUpdateArrow.setVisibility(View.VISIBLE);
        mCaptureButtonFirst.setVisibility(View.INVISIBLE);

        mUSBMonitor.unregister(); //USB Manager disassociate

        mBound = false;
        unbindService(binderConnection);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("@ @ @ ", "onDESTROY");
        if (mHandlerFirst != null) {
            mHandlerFirst = null;
        }

        if (mUSBMonitor != null) {
            mUSBMonitor.destroy();
            mUSBMonitor = null;
        }

        mUVCCameraViewFirst = null;
        mCaptureButtonFirst = null;

        super.onDestroy();
    }

    /**
     * to access from CameraDialog
     *
     * @return
     */
    @Override
    public USBMonitor getUSBMonitor() {
        return mUSBMonitor;
    }

    @Override
    public void onDialogResult(boolean canceled) {
        if (canceled) {
            runOnUiThread(this::setCameraButton, 0);
        }
    }

    private void setCameraButton() {
        Log.d("@ @ @", "mHandlerFirst: " + mHandlerFirst + " - mHandlerFirst.isOpened: " + mHandlerFirst.isOpened());
        runOnUiThread(() -> {
            if ((mHandlerFirst != null) && !mHandlerFirst.isOpened() && (mCaptureButtonFirst != null)) {
                mUpdateArrow.setVisibility(View.VISIBLE);
                mCaptureButtonFirst.setVisibility(View.INVISIBLE);
            }
        }, 0);
    }

    private void CaptureImage() {
        if (mHandlerFirst != null) {
            if (mHandlerFirst.isOpened()) {
                try {
                    if (checkPermissionWriteExternalStorage()) {
                        mHandlerFirst.captureStill();
                        Toast.makeText(MainActivity.this, "Image Captured", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap ConvertToBmp(byte[] yuvArray, int width, int height) {
        YuvImage yuvImage = new YuvImage(yuvArray, ImageFormat.YUY2, width, height, null);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, width, height), 100, os);
        byte[] jpegByteArray = os.toByteArray();
        try {
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
    }

    private void displayResolutionDialog() {
        String[] resolutions = resolutionsAvailable.keySet().toArray(new String[0]);

        AlertDialog.Builder resolutionBuilder = new AlertDialog.Builder(MainActivity.this);
        resolutionBuilder.setTitle("Select Camera Resolution");

        try {
            buttonPositiveInvolvement = resolutionDialog.getButton(Dialog.BUTTON_POSITIVE);
            buttonPositiveInvolvement.setEnabled(false);
        } catch(Exception ex) {
            Log.d("@ @ @ ", "Exception: " + ex);
        }

        resolutionBuilder.setSingleChoiceItems(resolutions, -1, (dialogInterface, i) -> {
            selectedResolution = resolutions[i];
            try{
                buttonPositiveInvolvement = resolutionDialog.getButton(Dialog.BUTTON_POSITIVE);
                buttonPositiveInvolvement.setEnabled(true);
            }catch (Exception ex){
               Log.d("@ @ @", "Exception: " + ex);
            }
        });

        resolutionBuilder.setPositiveButton("OK", (dialog, whichButton) -> {
            //  get Resolution parameters (width, height) and initCamera
            if(selectedResolution.length() != 0) {
                Pair<Integer, Integer> width_height = resolutionsAvailable.get(selectedResolution);
                if (width_height != null) {
                    initCamera(width_height.first, width_height.second);
                    dialog.dismiss();
                }
            } else {
                Toast.makeText(this, "Please select a Resolution!", Toast.LENGTH_SHORT).show();
            }
        });

        resolutionBuilder.setCancelable(false);

        resolutionDialog = resolutionBuilder.create();
        resolutionDialog.show();
        try {
            buttonPositiveInvolvement = resolutionDialog.getButton(Dialog.BUTTON_POSITIVE);
            buttonPositiveInvolvement.setEnabled(false);
        } catch(Exception ex) {
            Log.d("@ @ @ ", "Exception: " + ex);
        }

    }

    /*
    private void reLoadCamera(int w, int h) {

        myCloseCamera();
        myChangeResolution(w, h);
        myOpenCamera(deviceSaved, monitorSaved);
        myStartPreview();

        //myUpdateResolution(deviceSaved, monitorSaved, w, h);
    }

    private void myCloseCamera() {
        if(mHandlerFirst != null && mHandlerFirst.isOpened()) {
            mHandlerFirst.stopPreview();
            mHandlerFirst.close();

            if (mUVCCameraViewFirst.getSurface() != null) {
                mUVCCameraViewFirst.getSurface().release();
                mUVCCameraViewFirst.cancelPendingInputEvents();
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(mUSBMonitor != null) {
                mUSBMonitor.unregister();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mUSBMonitor.register();
            }
        } else Toast.makeText(this, "Camera is not active", Toast.LENGTH_SHORT).show();
    }

    private void myChangeResolution(int w, int h){
        myInitCamera(w, h);
    }

    private void myInitCamera(int width, int height) {

        mUVCCameraViewFirst = findViewById(R.id.camera_view_first);
        mUVCCameraViewFirst.setOnClickListener(mOnClickListener);

        mUVCCameraViewFirst.setAspectRatio(width / (float) height);

        // Set width and height of camera resolution
        mHandlerFirst = UVCCameraHandler.createHandler(this, mUVCCameraViewFirst
                , width, height, BANDWIDTH_FACTORS[0], secondDataCallBack);

    }

    private void myOpenCamera(UsbDevice device, UsbControlBlock ctrlBlock) {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mUSBMonitor.requestPermission(device);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (mUSBMonitor.hasPermission(device)) {
            if (!mHandlerFirst.isOpened()) {
                monitorSaved = ctrlBlock;       // it is a copy for re-load cameraHandler
                deviceSaved = device;           // it is a copy for re-load cameraHandler
                mHandlerFirst.open(ctrlBlock);

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (mHandlerFirst.isOpened()) {
                    contrBar.setProgress(mHandlerFirst.getValue(UVCCamera.PU_CONTRAST));
                    brightBar.setProgress(mHandlerFirst.getValue(UVCCamera.PU_BRIGHTNESS));
                }
            }
        }
    }

    private void myStartPreview() {

        runOnUiThread(() -> {
            mFirstPreviewSurface = mUVCCameraViewFirst.getSurfaceTexture();
            mHandlerFirst.startPreview(mFirstPreviewSurface);
            mUpdateArrow.setVisibility(View.INVISIBLE);
            mCaptureButtonFirst.setVisibility(View.VISIBLE);
        });
    }

    UvcCameraDataCallBack secondDataCallBack = data -> {
        if (DEBUG) Log.d("@ @ @ ", "secondDataCallBack:" + data.length);
    };

    public void myUpdateResolution(final UsbDevice device, final UsbControlBlock ctrlBlock, int width, int height) {
        if (SET_WIDTH == width && SET_HEIGHT == height) {
            return;
        }
        SET_WIDTH = width;
        SET_HEIGHT = height;
        if (mHandlerFirst != null) {
            mHandlerFirst.release();
            mHandlerFirst = null;
        }

        mUVCCameraViewFirst.setAspectRatio(SET_WIDTH / (float)SET_HEIGHT);
        mHandlerFirst = UVCCameraHandler.createHandler(this, mUVCCameraViewFirst
                ,width, height, 0.5f, secondDataCallBack);

        try {
            monitorSaved = ctrlBlock.clone();       // it is a copy for re-load cameraHandler
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        deviceSaved = device;

        mHandlerFirst.open(monitorSaved);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // wait for camera created
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // start previewing
                runOnUiThread(() -> {
                    Toast.makeText(MainActivity.this, "SLEEP", Toast.LENGTH_LONG).show();
                    myStartPreview();
                }, 0);
            }
        }).start();
    }
    */
}
